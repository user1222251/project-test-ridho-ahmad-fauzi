/** @type {import('tailwindcss').Config} */
export default {
	content: ['./index.html', './src/**/*.{js,ts,jsx,tsx}'],
	theme: {
		screens: {
			xl: { max: '1281px' },
			'2lg': { max: '1100px' },
			lg: { max: '1025px' },
			tab: { max: '841px' },
			md: { max: '769px' },
			sm: { max: '641px' },
		},
		extend: {},
	},
	plugins: [],
};
