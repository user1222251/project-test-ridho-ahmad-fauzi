import CardIdeas from '../components/CardIdeas';
import { format, parseISO } from 'date-fns';
import idLocale from 'date-fns/locale/id';
import { useState, useEffect } from 'react';
import axios from 'axios';
import ReactPaginate from 'react-paginate';

const CardIdeaContainer = () => {
	const [dataIdeas, setDataIdeas] = useState([]);
	const [loading, setLoading] = useState(false);
	const baseUrl = import.meta.env.VITE_APP_API_URL;
	const [pageSize, setPageSize] = useState(10);
	const [sortBy, setSortBy] = useState('published_at');
	const [pageNumber, setPageNumber] = useState(1);
	const [dataMeta, setDataMeta] = useState();
	useEffect(() => {
		const fetchData = async () => {
			try {
				setLoading(true);
				console.log(pageSize, sortBy);
				const url = `${baseUrl}?page[number]=${pageNumber}&page[size]=${pageSize}&append[]=medium_image&sort=${sortBy}`;
				const response = await axios.get(`${url}`, {
					headers: {
						'Content-Type': 'application/json',
					},
				});

				const data = response.data;
				if (response.status === 200) {
					setDataIdeas(data.data);
					setDataMeta(data.meta);
					console.log(dataMeta);
				}
			} catch (err) {
				console.error('Error fetching ideas:', err);
			} finally {
				setLoading(false);
			}
		};

		fetchData();
	}, [pageSize, sortBy, pageNumber]);

	const formatDate = (dateString) => {
		const parsedDate = parseISO(dateString);
		return format(parsedDate, 'dd MMMM yyyy', { locale: idLocale }).toUpperCase();
	};

	return (
		<>
			<section className="px-[8%] py-20 sm:py-12">
				<div className="mb-8 flex justify-between items-center md:gap-3 md:items-start md:flex-col">
					<div>
						<p>
							Showing 1-{pageSize || 0} of {dataMeta?.total || 0}
						</p>
					</div>
					<div className="flex gap-4 md:gap-3 md:flex-col">
						<div>
							<label htmlFor="total-list">Show per page: </label>
							<select
								onChange={(event) => setPageSize(event.target.value)}
								name="total-list"
								className="inline-block ml-2 min-w-[6rem] py-2 px-3 border-2 rounded-full"
								id="total-list"
							>
								<option value="10">10</option>
								<option value="20">20</option>
								<option value="50">50</option>
							</select>
						</div>
						<div>
							<label htmlFor="sort-by">Sort by: </label>
							<select
								onChange={(event) => setSortBy(event.target.value)}
								name="sort-by"
								className="inline-block ml-2 min-w-[8rem] py-2 px-3 border-2 rounded-full"
								id="sort-by"
							>
								<option value="published_at">Newest</option>
								<option value="-published_at">Latest</option>
							</select>
						</div>
					</div>
				</div>
				<div className="grid grid-cols-4 tab:grid-cols-3 sm:grid-cols-2 gap-5 sm:gap-3">
					{dataIdeas.map((idea, index) => (
						<CardIdeas
							key={index}
							image={idea.medium_image[0]?.url || 'https://picsum.photos/265/150'}
							date={formatDate(idea.published_at)}
							title={idea.title}
							effectLazy={loading ? 'blur' : ''}
						/>
					))}
				</div>
				<div className="flex justify-center mt-12">
					<ReactPaginate
						breakLabel="..."
						nextLabel=">>"
						onPageChange={(event) => setPageNumber(event.selected)}
						pageRangeDisplayed={5}
						pageCount={dataMeta?.last_page || 10}
						previousLabel="<<"
						renderOnZeroPageCount={null}
						className="flex font-semibold "
						pageLinkClassName="px-2 py-2"
						activeClassName="bg-orange-500 rounded-md text-white"
					/>
				</div>
			</section>
		</>
	);
};

export default CardIdeaContainer;
