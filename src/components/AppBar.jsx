import { useState, useEffect } from 'react';
import { NavLink, useLocation } from 'react-router-dom';
import useScrollListener from '../hooks/useScrollListener';
import { IoMenu } from 'react-icons/io5';
import { IoClose } from 'react-icons/io5';
import Bar from './Bar';

const AppBar = () => {
	const [navClassList, setNavClassList] = useState([]);
	const [open, setOpen] = useState(false);
	const scroll = useScrollListener();
	const location = useLocation(); // once ready it returns the 'window.location' object
	const [url, setUrl] = useState(null);

	useEffect(() => {
		setUrl(location.pathname);
	}, [location]);
	// update classList of nav on scroll
	useEffect(() => {
		const _classList = [];

		if (scroll.y > 100 && scroll.y - scroll.lastY > 0) _classList.push('hidden');

		setNavClassList(_classList);
	}, [scroll.y, scroll.lastY]);

	const ListNav = ({ path, titleNav }) => {
		return (
			<>
				<li className="hover:text-white">{titleNav}</li>
				<span className={`block w-full bg-white h-1 mt-1 rounded-md ${url === `${path}` ? 'block' : 'hidden'}`}></span>
			</>
		);
	};

	const listNavAttr = [
		{
			titleNav: 'Work',
			path: '/work',
		},
		{
			titleNav: 'About',
			path: '/about',
		},
		{
			titleNav: 'Services',
			path: '/services',
		},
		{
			titleNav: 'Ideas',
			path: '/ideas',
		},
		{
			titleNav: 'Careers',
			path: '/careers',
		},
		{
			titleNav: 'Contact',
			path: '/contact',
		},
	];

	return (
		<>
			<nav className={navClassList.join(' ') + ' w-full px-[8%] py-4 flex justify-between items-center bg-orange-500/90 fixed top-0 z-10 '}>
				<div className="">
					<img className="h-11" src="/images/suitmedia-white.png" alt="Logo Suitmedia" />
				</div>
				<div onClick={() => setOpen(!open)} className="hidden sm:block">
					{open ? <IoClose className="text-white text-3xl" /> : <IoMenu className="text-white text-3xl" />}
				</div>
				<div
					className={`sm:absolute sm:z-[-10] sm:py-6 sm:left-0 sm:w-full sm:bg-orange-500/90 ${
						open ? 'sm:top-[76px]' : 'sm:-top-[600px]'
					} transition-all ease-in-out duration-700`}
				>
					<ul
						className={`flex gap-5 text-white/80 sm:flex-col sm:items-center ${
							open ? 'sm:translate-y-0' : 'sm:-translate-y-32'
						} transition-all ease-in-out duration-300`}
					>
						{listNavAttr.map((attr) => (
							<NavLink key={attr.titleNav} to={attr.path}>
								<ListNav path={attr.path} titleNav={attr.titleNav} />
							</NavLink>
						))}
					</ul>
				</div>
			</nav>
		</>
	);
};
export default AppBar;
