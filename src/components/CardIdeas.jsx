import LinesEllipsis from 'react-lines-ellipsis';
import { Link } from 'react-router-dom';
import { LazyLoadImage } from 'react-lazy-load-image-component';

const CardIdeas = ({ image, date, title, effectLazy }) => {
	return (
		<>
			<Link>
				<div className="rounded-lg overflow-hidden shadow-[0px_0px_44px_-5px_rgba(0,0,0,0.10)] min-h-[320px] lg:min-h-[280px] tab:min-h-[300px] md:min-h-[280px] sm:min-h-[240px]">
					<div className="aspect-[3/2]">
						<LazyLoadImage alt={`Gambar ${title}`} src={image} className="w-full h-full object-cover object-center" effect={effectLazy} />
					</div>
					<div className="px-4 pt-7 sm:pt-6 sm:pb-4">
						<p className="text-sm sm:text-xs text-gray-400 font-medium">{date}</p>
						<h2 className="text-lg sm:text-base font-bold">
							<LinesEllipsis text={title} maxLine="3" ellipsis="..." trimRight basedOn="letters" />
						</h2>
					</div>
				</div>
			</Link>
		</>
	);
};

export default CardIdeas;
