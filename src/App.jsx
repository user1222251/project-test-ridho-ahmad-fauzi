import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Ideas from './pages/Ideas';
import Layout from './components/Layout';
import Other from './pages/Other';

function App() {
	return (
		<>
			<BrowserRouter>
				<Layout>
					<Routes>
						<Route path="/" element={<Ideas />} />
						<Route path="/ideas" element={<Ideas />} />
						<Route path="/work" element={<Other content={'Halaman Work'} />} />
						<Route path="/about" element={<Other content={'Halaman About'} />} />
						<Route path="/services" element={<Other content={'Halaman Services'} />} />
						<Route path="/careers" element={<Other content={'Halaman Careers'} />} />
						<Route path="/contact" element={<Other content={'Halaman Contact'} />} />
					</Routes>
				</Layout>
			</BrowserRouter>
		</>
	);
}

export default App;
