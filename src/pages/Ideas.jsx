import CardIdeaContainer from '../containers/CardIdeaContainer';
import { useParallax } from 'react-scroll-parallax';

const Ideas = () => {
	const parallax = useParallax({ scale: [1, 1.2] });

	return (
		<>
			<section className="relative w-full h-[550px]">
				<figure className="w-[150%] -rotate-6 absolute -top-[20%] -left-[20%] overflow-hidden">
					<img
						ref={parallax.ref}
						className="w-full h-[600px] cover object-center brightness-[0.6]"
						src="https://suitmedia.static-assets.id/storage/files/601/6.jpg"
						alt=""
					/>
				</figure>
				<div className="z-0 absolute top-[50%] left-[50%] translate-x-[-50%] translate-y-[-50%] text-white text-center">
					<h1 className="text-6xl">Ideas</h1>
					<p className="text-xl">Where all our great things begin</p>
				</div>
			</section>
			<CardIdeaContainer />
		</>
	);
};

export default Ideas;
