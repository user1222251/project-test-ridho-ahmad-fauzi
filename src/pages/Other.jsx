const Other = ({ content }) => {
	return (
		<>
			<section className="w-full h-screen grid place-content-center">
				<h1>{content}</h1>
			</section>
		</>
	);
};

export default Other;
